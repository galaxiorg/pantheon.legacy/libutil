-- Include function for Pantheon
local function include(library, file, ...)
	local argl = {...}
	local libraryPath = library:gsub(".", "/")
	local prefixes = {
		"/rom/apis/",
		"/rom/apis/command/",
		"/rom/apis/turtle/",
		"/boot/lib/",
		"/lib/",
		""
	}
	if _ARCH.pantheon then
		local ok, lib
		if file then
			for i, prefix in ipairs(prefixes) do
				ok, lib = loadfile("/bin/lln")("Il", file, prefix..libraryPath, "--")
				if ok then return lib end
			end
		end
		if not file or ok then
			local export
			for i, prefix in ipairs(prefixes) do
				if fs.exists(prefix..libraryPath) then
					export = loadfile(prefix..libraryPath)(table.unpack(argl))
				elseif fs.exists(prefix..libraryPath..".lua") then
					export = loadfile(prefix..libraryPath)(table.unpack(argl))
				end
			end
			return export
		end
	else
		local export
		for i, prefix in ipairs(prefixes) do
			if fs.exists(prefix..libraryPath) then
				export = loadfile(prefix..libraryPath)(table.unpack(argl))
			elseif fs.exists(prefix..libraryPath..".lua") then
				export = loadfile(prefix..libraryPath)(table.unpack(argl))
			end
		end
		return export
	end
end
