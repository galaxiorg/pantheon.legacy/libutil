-- Argument packer for Pantheon
local argt, argl, argn = table.pack(...), {}, 0
argn = argt.n
argl.n = nil
return argl, argn
