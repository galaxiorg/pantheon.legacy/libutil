-- Adds documentation at the side of the document, on a specified column
function getargs(t)
	local n = t.n
	t.n = nil
  return t,n
end
local nargl = table.pack(...)
local argl, argn = getargs(nargl)
if argn <= 1 then
	for i,arg in ipairs(argl) do print(arg) end
 	error("docside: At least two arguments are needed.")
end
local endlines = {}
local radd = false
for l in io.lines(argl[1]) do
	if l:match("--@dstart@--") then radd = true end
	if l:match("--@dend@--") then radd = false end
	l = l:gsub("\t", "  ")
	l = l:gsub("--@dstart@--", "--"..("="):rep(tonumber(argl[2])-4).."--")
	l = l:gsub("--@dend@--", "--"..("="):rep(tonumber(argl[2])-4).."--")
	local endline = l
	if not radd then
		local column = 0
		for c in l:gmatch(".") do column = column+1 end
		while column < tonumber(argl[2])-2 do
			column = column + 1
			endline = endline .. " "
		end
		endline = endline .. "--|"
	end
	table.insert(endlines, endline)
end
local file = io.open(argl[1], "w")
for i,l in pairs(endlines) do
	file:write(l.."\n")
end
file:close()
